function hello(param) {
    return param === undefined ? "Hello world!" : `Hello ${param}!`;
}
