function sum(array) {
    return array.reduce((a, b) => a + b);
}

function mean(array) {
    return sum(array) / array.length;
}
