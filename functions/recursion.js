function reverse(string) {
    if (string.length === 0) {
        return "";
    }

    return reverse(string.substr(1)) + string[0];
}
