function isThreeDigit(number) {
    return number >= 100 && number < 1000;
}
