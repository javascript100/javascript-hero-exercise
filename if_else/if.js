function equals(number1, number2) {
    return number1 === number2 ? "EQUAL" : "UNEQUAL";
}
