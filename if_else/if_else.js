function addWithSurcharge(a, b) {
    let surcharge = 0;

    if (a <= 10) {
        surcharge = surcharge + 1;
    } else {
        surcharge = surcharge + 2;
    }

    if (b <= 10) {
        surcharge = surcharge + 1;
    } else {
        surcharge = surcharge + 2;
    }

    return a + b + surcharge;
}
