let surcharge = 0;

function surchargeCalculator(number) {
    if (number <= 10) surcharge += 1;
    else if (number > 10 && number <= 20) surcharge += 2;
    else if (number > 20) surcharge += 3;
}

function addWithSurcharge(number1, number2) {
    surchargeCalculator(number1);
    surchargeCalculator(number2);

    return number1 + number2 + surcharge;
}
