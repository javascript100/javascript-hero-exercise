function repdigit(n) {
    let onesDigit = n % 10;
    let tensDigit = Math.floor(n / 10);
    return onesDigit === tensDigit ? "Repdigit!" : "No Repdigit!";
}
