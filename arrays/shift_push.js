function rotate(array) {
    const firstElement = array.shift();
    array.push(firstElement);
    return array;
}
