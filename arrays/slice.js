function halve(array) {
    return array.slice(0, Math.ceil(array.length / 2));
}
