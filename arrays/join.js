function list(array) {
    const length = array.length;

    if (length === 0) return "";
    if (length === 1) return array[0];

    const lastElement = array[length - 1];
    const rest = array.slice(0, length - 1);
    return `${rest.join(", ")} and ${lastElement}`;
}
