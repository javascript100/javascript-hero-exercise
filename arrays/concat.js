function concatUp(array1, array2) {
    const array1Length = array1.length;
    const array2Length = array2.length;
    return array1Length === array2Length
        ? array1.concat(array2)
        : (
            array1Length > array2Length
            ? array2.concat(array1)
            : array1.concat(array2)
        );
}
