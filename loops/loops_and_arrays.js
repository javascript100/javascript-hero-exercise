function mean(array) {
    let sum = 0, length = array.length;
    for (let i = 0; i < length; i++) {
        sum += array[i];
    }
    return sum / length;
}
