function lcm(num1, num2) {
    let lcm = 0;
    let rem1 = 0,
        rem2 = 0;
    do {
        lcm++;
        rem1 = lcm % num1;
        rem2 = lcm % num2;
    } while (rem1 !== 0 || rem2 !== 0);
    return lcm;
}
