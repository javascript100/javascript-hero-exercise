function isPrime(number) {
    if (number === 1) return false;

    let flag = true;
    for (let i = 2; i < number; i++) {
        if (number % i === 0) {
            flag = false;
            break;
        }
    }

    return flag;
}
