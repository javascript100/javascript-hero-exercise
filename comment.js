// Single line comment

/*
    Block comment
    covering 2 lines
*/

function median(array) {
    const length = array.length;
    if (length === 0) return 0;
    if (length === 1) return array[0];

    const middle = Math.floor(length / 2);
    return length % 2 === 0
        ? (array[middle - 1] + array[middle]) / 2
        : array[middle];
}
