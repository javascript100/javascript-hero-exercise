function whereIs(name) {
    return "Where is " + name + "?";
}

let x = whereIs("Jacky");

// answer: 'Where is Jacky?'
