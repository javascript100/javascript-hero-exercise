function double(name) {
    return name + " and " + name;
}

let x = double("Roy");

// answer: 'Roy and Roy'
