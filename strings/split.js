function add(string) {
    let numbers = string.split("+");

    let sum = 0;
    for (let i = 0; i < numbers.length; i++) {
        sum += parseInt(numbers[i]);
    }

    return sum;
}
