const roman_numerals = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
};

function arabic(roman) {
    let result = 0;
    let roman_number_array = roman.split("");

    for (let i = 0; i < roman_number_array.length; i++) {
        let current_roman_number = roman_number_array[i];
        let next_roman_number = roman_number_array[i + 1];

        if (
            next_roman_number &&
            roman_numerals[next_roman_number] > roman_numerals[current_roman_number]
        ) {
            result += (-roman_numerals[current_roman_number]);
        } else {
            result += (roman_numerals[current_roman_number]);
        }
    }

    return result;
}
