function toFahrenheit(celsius) {
    return 1.8 * celsius + 32;
}
