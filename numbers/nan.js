function parseFirstInt(string) {
    let stringCopy = string;

    for (let i = 0; i < string.length; i++) {
        const firstInteger = parseInt(stringCopy);
        if (!Number.isNaN(firstInteger)) 
            return firstInteger;
        stringCopy = stringCopy.substr(1);
    }

    return NaN;
}
