function hypotenuse(a, b) {
    let cSquared = Math.pow(a, 2) + Math.pow(b, 2);
    return Math.sqrt(cSquared);
}
