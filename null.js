function cutComment(string) {
    const index = string.indexOf("//");
    return index === -1 ? null : string.slice(index + 3, string.length);
}
